package com.example.juanp.musicplayer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    //Boolean for MainActivity Adapter
    private boolean mainActivity;

    //Arraylist for the list
    private ArrayList<Song> listAllSongs;
    private ArrayList<Song> listTopHits;
    private ArrayList<Song> listRock;
    private ArrayList<Song> listPunk;

    //RecyclerViews
    private RecyclerView recyclerTopHits;
    private RecyclerView recyclerRock;
    private RecyclerView recyclerPunk;

    //Layouts managers
    private RecyclerView.LayoutManager layoutManagerTopHits;
    private RecyclerView.LayoutManager layoutManagerRock;
    private RecyclerView.LayoutManager layoutManagerPunk;

    //Adapters
    private CustomAdapterList adapterTopHits;
    private CustomAdapterList adapterRock;
    private CustomAdapterList adapterPunk;

    //Buttons for bottom menu
    private ImageView imagePlayerButton;
    private ImageView imageSearchButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Set boolean to true
        mainActivity = true;

        //Method to list songs in the arrays
        listSongs();
        //Method to build the recyclerviews
        buildRecyclerView();

        //Assign the image buttons
        imagePlayerButton = findViewById(R.id.img_player_button);
        imageSearchButton = findViewById(R.id.img_search_button);

        //Assign onclicklistener to the buttons
        imagePlayerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Intent adding the ArrayList and a default song
                Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
                intent.putExtra("Song", listAllSongs.get(0));
                intent.putParcelableArrayListExtra("listAllSongs",(ArrayList <Song>) listAllSongs);
                startActivity(intent);
                finish();
            }
        });

        imageSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Intent adding the ArrayList
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                intent.putParcelableArrayListExtra("listAllSongs",(ArrayList <Song>) listAllSongs);
                startActivity(intent);
                finish();
            }
        });
    }

    //Add songs to the arraylist
    private void listSongs() {
        listAllSongs = new ArrayList<>();
        listAllSongs.add(new Song(true, getString(R.string.rock), "Guns N'Roses", "Sweet Child O' Mine", R.drawable.album_gunsnroses_apettitefordestruction, 0));
        listAllSongs.add(new Song(false, getString(R.string.punk), "Blink-182", "Dummit", R.drawable.album_blink182_duderanch, 1));
        listAllSongs.add(new Song(false, getString(R.string.rock), "Sum 41", "In too deep", R.drawable.album_sum41_allkillernofiller, 2));
        listAllSongs.add(new Song(true, getString(R.string.punk), "Blink-182", "All the Small Things", R.drawable.album_blink182_enemaofthestate, 3));
        listAllSongs.add(new Song(true, getString(R.string.rock), "Guns N'Roses", "Paradise City", R.drawable.album_gunsnroses_apettitefordestruction, 4));
        listAllSongs.add(new Song(false, getString(R.string.punk), "Blink-182", "First Date", R.drawable.album_blink182_takeoffyourpantsandjacket, 5));
        listAllSongs.add(new Song(false, getString(R.string.rock), "Sum 41", "Fat Lip", R.drawable.album_sum41_allkillernofiller, 6));


        //Methods to list the genders of music
        listTopHits();
        listRock();
        listPunk();
    }

    //Methods to get the arrays of the differents gender of music
    private void listTopHits() {

        listTopHits = new ArrayList<>();
        //Bucle for to list the song if it is topHit
        for (int i = 0; i < listAllSongs.size(); i++) {
            if (listAllSongs.get(i).isTopHit()) {
                listTopHits.add(listAllSongs.get(i));
            }
        }
    }

    private void listRock() {

        listRock = new ArrayList<>();
        //String to check the gender
        String checkGender = getString(R.string.rock);
        //Bucle for to list the song if it is rock
        for (int i = 0; i < listAllSongs.size(); i++) {
            if (listAllSongs.get(i).getGender().equalsIgnoreCase(checkGender)) {
                listRock.add(listAllSongs.get(i));
            }
        }
    }

    private void listPunk() {

        listPunk = new ArrayList<>();
        //String to check the gender
        String checkGender = getString(R.string.punk);
        //Bucle for to list the song if it is rock
        for (int i = 0; i < listAllSongs.size(); i++) {
            if (listAllSongs.get(i).getGender().equalsIgnoreCase(checkGender)) {
                listPunk.add(listAllSongs.get(i));
            }
        }
    }

    private void buildRecyclerView() {

        //Methods to build recyclerview songs for each gender of music
        buildTopHits();
        buildRock();
        buildPunk();

    }

    private void buildTopHits() {

        //Set Layout Manager
        layoutManagerTopHits = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        //TopHits
        adapterTopHits = new CustomAdapterList(listTopHits,mainActivity);
        recyclerTopHits = findViewById(R.id.RecyclerTopHits);
        recyclerTopHits.setHasFixedSize(true);
        recyclerTopHits.setLayoutManager(layoutManagerTopHits);
        recyclerTopHits.setAdapter(adapterTopHits);

        //Top hits clicklistener
        adapterTopHits.setOnItemClickListener(new CustomAdapterList.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
                intent.putExtra("Song", listTopHits.get(position));
                intent.putParcelableArrayListExtra("listAllSongs",(ArrayList <Song>) listAllSongs);
                startActivity(intent);
            }
        });

    }

    private void buildRock() {

        //Set Layout Manager
        layoutManagerRock = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        //Rock
        adapterRock = new CustomAdapterList(listRock,mainActivity);
        recyclerRock = findViewById(R.id.RecyclerRock);
        recyclerRock.setHasFixedSize(true);
        recyclerRock.setLayoutManager(layoutManagerRock);
        recyclerRock.setAdapter(adapterRock);

        //Rock clicklistener
        adapterRock.setOnItemClickListener(new CustomAdapterList.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
                intent.putExtra("Song", listRock.get(position));
                intent.putParcelableArrayListExtra("listAllSongs",(ArrayList <Song>) listAllSongs);
                startActivity(intent);
            }
        });
    }

    private void buildPunk() {

        //Set Layout Manager
        layoutManagerPunk = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        //Punk
        adapterPunk = new CustomAdapterList(listPunk,mainActivity);
        recyclerPunk = findViewById(R.id.RecyclerPunk);
        recyclerPunk.setHasFixedSize(true);
        recyclerPunk.setLayoutManager(layoutManagerPunk);
        recyclerPunk.setAdapter(adapterPunk);

        //Punk clicklistener
        adapterPunk.setOnItemClickListener(new CustomAdapterList.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
                intent.putExtra("Song", listPunk.get(position));
                intent.putParcelableArrayListExtra("listAllSongs",(ArrayList <Song>) listAllSongs);
                startActivity(intent);
            }
        });
    }
}
