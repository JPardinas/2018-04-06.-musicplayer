package com.example.juanp.musicplayer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PlayerActivity extends AppCompatActivity {

    //Arraylist for the list of songs
    private int songNumber;
    private ArrayList<Song> listAllSongs;

    //Boolean for play button
    private boolean buttonPlayPause;

    //Initialize TextView
    private TextView textArtist;
    private TextView textSong;

    //Initialize ImageView
    private ImageView imgAlbum;
    private ImageView imgPreviousButton;
    private ImageView imgPlayPauseButton;
    private ImageView imgNextButton;

    //Buttons for bottom menu
    private ImageView imageHomeButton;
    private ImageView imageSearchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        //Assign the image buttons
        imageHomeButton = findViewById(R.id.img_home_button);
        imageSearchButton = findViewById(R.id.img_search_button);

        //Assign variables for song info
        textArtist = (TextView) findViewById(R.id.textArtist);
        textSong = (TextView) findViewById(R.id.textSong);
        imgAlbum = (ImageView) findViewById(R.id.imgAlbum);

        //Assign variables for the player buttons
        imgPreviousButton = (ImageView) findViewById(R.id.img_previous_button);
        imgPlayPauseButton = (ImageView) findViewById(R.id.img_play_pause_button);
        imgNextButton = (ImageView) findViewById(R.id.img_next_button);

        //Set the button status
        buttonPlayPause = true;

        //Get the song and put in to the player, get the number of the song too (to know if the player can switch to another song)
        songNumber = setGettedSong();

        //Get the arraylist of all the songs
        getAllSongs();

        //Assign onclicklistener to the buttons
        imageHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Intent
                Intent intent = new Intent(PlayerActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        imageSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Intent adding the ArrayList
                Intent intent = new Intent(PlayerActivity.this, SearchActivity.class);
                intent.putParcelableArrayListExtra("listAllSongs",(ArrayList <Song>) listAllSongs);
                startActivity(intent);
                finish();
            }
        });

        imgPreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //If to check if there are more songs, to change or not
                if (songNumber > 0){
                    songNumber--;
                    //Set the song info in the player
                    textArtist.setText(listAllSongs.get(songNumber).getArtist());
                    textSong.setText(listAllSongs.get(songNumber).getSong());
                    imgAlbum.setImageResource(listAllSongs.get(songNumber).getImgAlbum());
                }
            }
        });

        imgPlayPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Switch play and pause button
                if (buttonPlayPause){
                    imgPlayPauseButton.setImageResource(R.mipmap.ic_pause);
                    buttonPlayPause = false;
                }else {
                    imgPlayPauseButton.setImageResource(R.mipmap.ic_play);
                    buttonPlayPause = true;
                }
            }
        });

        imgNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //If to check if there are more songs, to change or not
                if (songNumber < listAllSongs.size()-1){
                    songNumber++;
                    //Set the song info in the player
                    textArtist.setText(listAllSongs.get(songNumber).getArtist());
                    textSong.setText(listAllSongs.get(songNumber).getSong());
                    imgAlbum.setImageResource(listAllSongs.get(songNumber).getImgAlbum());
                }
            }
        });
    }

    //Get and set the clicked song
    private int setGettedSong() {

        //Intent to get the song
        Intent intent = getIntent();
        Song gettedSong = intent.getParcelableExtra("Song");

        //Get the info and set in to the values
        int imageRes = gettedSong.getImgAlbum();
        String artist = gettedSong.getArtist();
        String song = gettedSong.getSong();

        //Change the info of the player to the getted song
        imgAlbum.setImageResource(imageRes);
        textArtist.setText(artist);
        textSong.setText(song);

        //Get and return number of song
        int a;
        a = gettedSong.getNumberSong();
        return a;
    }

    //Get all songs in to the arraylist
    private void getAllSongs() {

        //Intent to get the arraylist
        listAllSongs = new ArrayList<Song>();
        listAllSongs = getIntent().getParcelableArrayListExtra("listAllSongs");
    }

}
