package com.example.juanp.musicplayer;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapterList extends RecyclerView.Adapter<CustomAdapterList.ViewHolderList> {

    //Arraylist and ClickHandler initialize
    private ArrayList<Song> listAllSongs;
    private OnItemClickListener mListener;
    private boolean mainActivity;

    //The interface that receives onClick messages.
    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    //Set listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }


    public static class ViewHolderList extends RecyclerView.ViewHolder {

        //Initialize variables
        private TextView textArtist;
        private TextView textSong;
        private ImageView imgAlbum;

        public ViewHolderList(View itemView, final OnItemClickListener listener) {

            super(itemView);
            imgAlbum = itemView.findViewById(R.id.imgAlbum);
            textArtist = itemView.findViewById(R.id.textArtist);
            textSong = itemView.findViewById(R.id.textSong);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    //Constructor
    public CustomAdapterList(ArrayList<Song> listAllSongs, boolean mainActivity) {
        this.listAllSongs = listAllSongs;
        this.mainActivity = mainActivity;
    }

    @NonNull
    @Override
    public ViewHolderList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //Set LayoutInflater - Use boolean mainactivity to know what layour we must use

        if (mainActivity){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_main_activity, parent, false);
            ViewHolderList evh = new ViewHolderList(v, mListener);
            return evh;
        }else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_search_activity, parent, false);
            ViewHolderList evh = new ViewHolderList(v, mListener);
            return evh;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderList holder, int position) {

        //Get the position
        Song song = listAllSongs.get(position);
        //Set the info
        holder.textArtist.setText(song.getArtist());
        holder.textSong.setText(song.getSong());
        holder.imgAlbum.setImageResource(song.getImgAlbum());
    }

    @Override
    public int getItemCount() {

        //Get number of items
        return listAllSongs.size();
    }

    //Filter
    public void filterList(ArrayList<Song> filteredList){

        listAllSongs = new ArrayList<>();
        listAllSongs.addAll(filteredList);
        notifyDataSetChanged();

    }
}
