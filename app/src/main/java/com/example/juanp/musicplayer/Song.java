package com.example.juanp.musicplayer;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Song implements Parcelable{

    //Declaration of the variables
    private boolean topHit;
    private String gender;
    private String artist;
    private String song;
    private int imgAlbum;
    private int numberSong;

    //Constructor
    public Song(boolean topHit, String gender, String artist, String song, int imgAlbum, int numberSong) {
        this.topHit = topHit;
        this.gender = gender;
        this.artist = artist;
        this.song = song;
        this.imgAlbum = imgAlbum;
        this.numberSong = numberSong;
    }


    //Parcelable
    protected Song(Parcel in) {
        topHit = in.readByte() != 0;
        gender = in.readString();
        artist = in.readString();
        song = in.readString();
        imgAlbum = in.readInt();
        numberSong = in.readInt();
    }

    public static final Creator<Song> CREATOR = new Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (topHit ? 1 : 0));
        dest.writeString(gender);
        dest.writeString(artist);
        dest.writeString(song);
        dest.writeInt(imgAlbum);
        dest.writeInt(numberSong);
    }

    //Gets
    public boolean isTopHit() {
        return topHit;
    }

    public String getGender() {
        return gender;
    }

    public String getArtist() {
        return artist;
    }

    public String getSong() {
        return song;
    }

    public int getImgAlbum() {
        return imgAlbum;
    }

    public int getNumberSong() {
        return numberSong;
    }
}
