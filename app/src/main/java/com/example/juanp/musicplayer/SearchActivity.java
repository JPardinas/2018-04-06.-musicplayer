package com.example.juanp.musicplayer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    //Boolean for MainActivity Adapter
    private boolean mainActivity;

    //Arraylist of the songs
    private ArrayList<Song> listAllSongs;

    //RecyclerViews
    private RecyclerView recyclerListAllSongs;

    //Adapters
    private CustomAdapterList adapterSearch;

    //Buttons for bottom menu
    private ImageView imageHomeButton;
    private ImageView imagePlayerButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //Assign the image buttons
        imagePlayerButton = findViewById(R.id.img_player_button);
        imageHomeButton = findViewById(R.id.img_home_button);

        //Assign onclicklistener to the buttons
        imageHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Intent
                Intent intent = new Intent(SearchActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        imagePlayerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Intent adding the ArrayList and a default song
                Intent intent = new Intent(SearchActivity.this, PlayerActivity.class);
                intent.putExtra("Song", listAllSongs.get(0));
                intent.putParcelableArrayListExtra("listAllSongs",(ArrayList <Song>) listAllSongs);
                startActivity(intent);
                finish();
            }
        });


        //EditText with the methods to filter the list
        EditText editText = findViewById(R.id.edit_search);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            //Method to filter after the text changes
            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());

            }
        });

        //Set boolean to true
        mainActivity = false;

        //Get the arraylist of all the songs
        getAllSongs();

        //Method to build the recyclerviews
        buildRecyclerView();

    }
    //Method to filter the list comparing song names
    private void filter(String text) {

        final ArrayList<Song> filteredList = new ArrayList<>();

        for (Song songName : listAllSongs) {
            if (songName.getSong().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(songName);
            }
        }

        //Set the adapter for recycleview
        adapterSearch = new CustomAdapterList(filteredList, mainActivity);
        recyclerListAllSongs.setAdapter(adapterSearch);

        //Clicklistener
        adapterSearch.setOnItemClickListener(new CustomAdapterList.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(SearchActivity.this, PlayerActivity.class);
                intent.putExtra("Song", filteredList.get(position));
                intent.putParcelableArrayListExtra("listAllSongs", (ArrayList<Song>) listAllSongs);
                startActivity(intent);
            }
        });
    }

    //Get all songs in to the arraylist
    private void getAllSongs() {

        //Intent to get the arraylist
        listAllSongs = new ArrayList<Song>();
        listAllSongs = getIntent().getParcelableArrayListExtra("listAllSongs");
    }

    //Method to create the list
    private void buildRecyclerView() {

        //Recycler for the lists
        recyclerListAllSongs = (RecyclerView) findViewById(R.id.RecyclerSearch);
        recyclerListAllSongs.setHasFixedSize(true);
        recyclerListAllSongs.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        //Set the adapter for the recycleview
        adapterSearch = new CustomAdapterList(listAllSongs, mainActivity);
        recyclerListAllSongs.setAdapter(adapterSearch);

        //Clicklistener
        adapterSearch.setOnItemClickListener(new CustomAdapterList.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(SearchActivity.this, PlayerActivity.class);
                intent.putExtra("Song", listAllSongs.get(position));
                intent.putParcelableArrayListExtra("listAllSongs", (ArrayList<Song>) listAllSongs);
                startActivity(intent);
            }
        });
    }

}
